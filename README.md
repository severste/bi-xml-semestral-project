# Semestrální práce na BI-XML
- Semestrální práce na předmět BI-XML na Fakultě informačních technologií ČVUT 2019
- **Autor**: Štěpán Severa
## Potřebný software
- xmllint
- jing
- fop
# Adresářová struktura
- data
  - Obsahuje xml soubory pro jednotlivé oblasti a obrázky
  - Oblasti:
    - Austria, Denmark, Germany, Sweden
  - XML soubory jsem vytvářel ručně, proto v nich nejsou uvedeny všechny informace, které byly na stránce uvedeny
  - Zdroj dat: https://www.cia.gov/library/publications/resources/the-world-factbook/
- html
  - Do tohoto odresáře se vygeneruje HTML výstup
  - Obsahuje css soubor, který definuje vzhled výsledných stránek
  - Na stránku se přistupuje přes soubor index.html
- transormation
  - Obsahuje xsl soubory, které definují transformace
  - HTML transformace
    - transformIndex.xsl definuje vygenerování index.html stránky, která odkazuje na stránky jednotlivých oblastí
    - transformPages.xsl vygeneruje jednu stránku pro každou oblast
  - PDF transformace
    - transformFO.xsl definuje vygenerování výslendého PDF souboru
- validation
  - obsahuje DTD a Relax NG soubory, které validují strukturu XML dokumentu
- slouceni.xml
  - definuje sloučení všech xml souborů ze složky data do jednoho souboru countries.xml
- countries.pdf
  - vygenerované pdf
- countries.xml
  - soubor vzniklý spojením všech souborů pomocí slouceni.dtd
  - na začátku se vygeneruje z xml souborů a poté se pracuje jen s ním, už ne s jednotlivými xml soubory ve složce data
- projekt_zadani_severa.xml
  - soubor, kde je zadání mé práce, kde jsou definované oblasti, které budu zpracovávat
- BI-XML-PROJEKT-INSTRUKCE.txt
  - soubor obsahující zadání semestrální práce
- BI-XML-PROJEKT-PREZENTACE.txt
  - soubor upřesňující výslednou formu semestrální práce

# Postup
- Přesuneme se do kořenového adresáře tohoto projektu - zde se nachází soubor README.md
- Všechny příkazy jsou spoušteny z tohoto adresáře
## 1. Sloučení do 1 XML
```
xmllint --dropdtd --noent --output countries.xml 'slouceni.xml'
```

## 2. Validace
### DTD
```
xmllint --noout --dtdvalid 'schema.dtd' countries.xml
```

### Relax NG
```
jing -c ./validation/schema.rnc countries.xml 
```

## 3. Vygenerování HTML
```
# vygenerovani index.html stranky
java -jar ~/Saxon/saxon9he.jar countries.xml ./transformation/transformIndex.xsl
# vygenerovani stranky pro kazdou zemi
java -jar ~/Saxon/saxon9he.jar countries.xml ./transformation/transformPages.xsl
```
- vše se vytvoří ve složce html, kde se nachazí soubor main.css, který definuje kaskádové styly pro danou stránku

## 4. Vygenerování PDF
```
~/fop-2.3/fop/fop -xml countries.xml -xsl transformation/transformFO.xsl -pdf countries.pdf
```
