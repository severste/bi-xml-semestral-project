<xsl:stylesheet version = '1.0' 
    xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

    <xsl:output omit-xml-declaration='yes' indent='yes'/>

    <xsl:template match="/">
        <xsl:result-document href="html/index.html">
            <!--<!DOCTYPE html>-->
            <html>
                <head>
                    <title>Countries</title>
                    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
                </head>
                <body>
                    <header>
                        <h1>Countries</h1>
                        <ul>
                            <xsl:apply-templates mode="menu"/>
                        </ul>
                    </header>
                    <main>
                        <div>
                            <p>
                            This is the main page of my semestral work for BI-XML FIT ČVUT 2019
                            </p>
                        </div>
                    </main>
                    <footer>
                        <p>Autor: Štěpán Severa, semestrální projekt BI-XML FIT ČVUT 2019</p>
                    </footer>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="//country" mode="menu">
        <li>
            <a href="{@name}.html">
                <xsl:value-of select="@name"/>
            </a>
        </li>
    </xsl:template>

</xsl:stylesheet>